# Example projects to try things out on

Note: Doesn't use `lerna` but it should.

![](http://pestleanalysis.com/wp-content/uploads/2011/12/examples-of-pestle-analysis.jpg)

--- 

| Example Package | Port |
|:--------:|:------------:|
| Caasette Deck | `4040` |
| Jukebox | `5050` |

---
## How to run
Open 3 terminals.

Run start/dev in each of the the folders. Do stuff in any of the folders.

* `example-caasette`: `npm run watch` (needs to still copy files)
* `example-deck`: `npm run dev`
* `example-jukebox`: `npm run start`
