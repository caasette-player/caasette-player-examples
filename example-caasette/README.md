## NOTES
Started from: https://github.com/psychobolt/react-rollup-boilerplate

A `caasette` is a bundled React Component that contains everything that is reguired for it to work within the `caasette deck`. But, also this repo isn't about building single `caasettes` either as you can have multiple packages.

It makes sense to be able to create packages & components (to logically separate concerns) into their own modules/packages and include them in other components or even a `caasette`. This allows you to publish a `caasette` or single package to be used elsewhere. Again, the only difference is the end result and last home.

To determine this it is done within the `package.json` within each package. Set a property within as `"caasette": "true|false"`. 

If `"caasette": "true"` then it and it's resources will be bundled together before being sent off to "wherever". I don't think a `caasette` will typically be published to npm.

If `"caasette": "false"` then we won't package this as a `caasette` and may be published to NPM. If another property exists of `"pushout": "true"` then it can also be published to NPM or artifactory. Meaning that it'll be built; otherwise it will not and it's code will only live within this repo and be used (even with global imports) by packages within here.

---

# Blank Tape
[![Dependencies Status](https://david-dm.org/psychobolt/react-rollup-boilerplate.svg)](https://david-dm.org/psychobolt/react-rollup-boilerplate)
[![Dev Dependencies Status](https://david-dm.org/psychobolt/react-rollup-boilerplate/dev-status.svg)](https://david-dm.org/psychobolt/react-rollup-boilerplate?type=dev)
[![Peer Dependencies Status](https://david-dm.org/psychobolt/react-rollup-boilerplate/peer-status.svg)](https://david-dm.org/psychobolt/react-rollup-boilerplate?type=peer)

[![Build Status](https://travis-ci.org/psychobolt/react-rollup-boilerplate.svg?branch=master)](https://travis-ci.org/psychobolt/react-rollup-boilerplate)
[![codecov](https://codecov.io/gh/psychobolt/react-rollup-boilerplate/branch/master/graph/badge.svg)](https://codecov.io/gh/psychobolt/react-rollup-boilerplate)

A boilerplate for building React libraries.

## Included

- [React](https://reactjs.org/) with [recompose](https://github.com/acdlite/recompose)
- [Rollup](https://rollupjs.org/) with [Babel](https://www.npmjs.com/package/rollup-plugin-babel), [SCSS](https://www.npmjs.com/package/rollup-plugin-scss) and other plugins:
    - [Node Resolve](https://www.npmjs.com/package/rollup-plugin-node-resolve)
    - [CommonJS](https://www.npmjs.com/package/rollup-plugin-commonjs)
    - [Uglify](https://www.npmjs.com/package/rollup-plugin-uglify)
    - [Alias](https://www.npmjs.com/package/rollup-plugin-alias)
- [styled-components](https://www.styled-components.com/) with [default](https://www.styled-components.com/docs/tooling#stylelint) [stylelint](https://stylelint.io/) support
- Monorepo support with [Lerna](https://lernajs.io)
- Code Coverage reporting with [Codecov](https://codecov.io/)
- Dev sandbox and documentation with [Storybook](https://storybook.js.org/)
- Structural and interaction testing with [Enzyme](https://github.com/airbnb/enzyme)
- Type checking with [Flow](https://flow.org)
- JS style check with [ESLint](http://eslint.org/) using [AirBnb style guide](https://github.com/airbnb/javascript)

## Including NPM packages

```sh
yarn add <package-name> --dev # for dev tools, story dependencies, libraries to be bundled
yarn add <package-name> [--peer] # for external dependencies (Note: Include in externals from rollup.config.common.js whenever update)
yarn lerna add <package-name> [--dev] [--peer] [packages/<target-package-name>] # Add/link a package to a sub-package. See section: Including sub-packages
```

## Including local packages

This boilerplate supports [Monorepo](https://danluu.com/monorepo/) configurations out of the box and will watch, build, serve any local packages. Each package should have ```src/index.js``` entry file.

By default, local packages are [independently](./lerna.json#L6) versioned. You may import your own repos with Lerna or create your own sub-packages using NPM:

```sh
yarn lerna import <path-to-external-repository> # import a repository to packages/
# or 
mkdir packages/<my-package> && cd <my-package> && yarn init
```

See Lerna's offical [readme](https://github.com/lerna/lerna#readme) for a configuration and usage guide.

> By default, the ```lerna.json``` defines the parent package at the [root](./lerna.json#L3). You may opt-out of this configuration manually, by removing its settings and any alias references to its directory or package. 

> You can also give alias to source files of the packages in order to work with Visual Studio Code's Intellisense. See [jsconfig.json](./jsconfig.json) and [usage](https://code.visualstudio.com/docs/languages/jsconfig#_using-webpack-aliases).
