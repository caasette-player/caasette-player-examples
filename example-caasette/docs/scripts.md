## Static Types

  ```sh
yarn flow # performs type checking on files
```

## Lint

  ```sh
yarn lint # runs linter to detect any style issues (css & js)
yarn lint:css # lint only css
yarn lint:js # lint only js
yarn lint:js --fix # attempts to fix js lint issues
```

## Test

  ```sh
yarn test # runs functional/unit tests using Jest
yarn test --coverage # with coverage
```

  > You can also inspect tests in debug mode within Visual Studio Code.

## Other scripts

  ```sh
yarn build # builds all packages
yarn build:dev [--environment PACKAGES:<*,package-name>] # builds sources for development, optionally provide environment variable to specify local package(s) e.g. yarn build:dev --environment PACKAGES:default-export,package-* (glob pattern supported)
yarn build:prod [--environment PACKAGES:<*,package-name>] # builds sources for production
yarn watch [--environment PACKAGES:<*,package-name>]# watches dev builds
yarn dist # builds all packages and publishes to npm
```