# TODO.md
Everything is compiled to a `Caasette` so everything (except source maps for
the dev `Caasette`) go in the top-level `dist/` directory not the `packages/`
directory. If engineers plan to use this to build dual purpose [contains a
regular UMD/CJS/etc] module then they'll need to update their configs
themselves.

- [x] Remove `src/` directory and instead only rely on the `packages/` directory.
- [x] Add `README.md` to the `'packages/` directory
- [x] Move all stories to the `src/` dir not above
- [ ] Move `flow` stuff to own package?
		- Or is that too much?
- [x] Add a default `manifest.yml` to every package at top-level
	- [x] `version`
	- [x] `name`
	- [x] `experiment`
- [ ] Move all the rollup stuff to it's own package
	- [ ] Make this include the `plop` files
	- [ ] Move storybook config to it's own package (Record Store)
- [ ] Something, anything, other than Jest
- [ ] Move these things to their own packages:
	- [ ] Package creation stuff to it's own package
	- [ ] The rollup configuration
	- [ ] `plop` stuff
	- [ ] A package to create more packages inline.
	- [ ] Move eslint stuff to own package
	- [ ] Move flow to own package
	- [ ] Create a babel preset along the lines of docz' one
	- [ ] **_Any of these packages that contain configs should be overrideable and placed in the `config/` directory with commented out & in-line documented configurations_**
- [ ] If `caasette` contains an image then the `caasette` and it's related resources should be bundled up together as a zip and the resolver can figure out how to handle it.
- [ ] Make all this shit fucking functional BROOO
- [ ] Proptypes are broken and don't translate well. Must fix.