/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
/* eslint-disable no-alert */
/* eslint-disable no-param-reassign */
/* eslint-disable react/button-has-type */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-unused-prop-types */
import React from 'react'

const clicked = () => { console.log('clicked') }
const cancel = () => { console.log('cancel') }
const submit = () => { console.log('submit') }

const FormExample = () => (
  <div>
    <div>
      <label htmlFor="answer" onClick={clicked}>
        Sorry not sorry that these elements don't work
        <input
          type="checkbox"
          onChange={clicked}
          defaultValue={question.value}
          name="label-text"
        />
      </label>
    </div>

    <textarea
      placeholder="Text?"
      value={feedbackText}
      onChange={clicked}
    />

    <div>
      <button onClick={cancel}>Cancel</button>
      <button onClick={submit}>Submit</button>
    </div>
  </div>
)

export { FormExample }
export default FormExample
