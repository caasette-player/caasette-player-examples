import React from 'react'

const buttonClick = () => alert('You clicked the button')

export default () => (
  <button onClick={() => buttonClick()}>
    Button
  </button>
)