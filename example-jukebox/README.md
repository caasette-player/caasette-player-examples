# Jukebox

> A simple `caasette-player/jukebox` implimentation.

![](https://i.imgur.com/IRIWQcy.jpg)

Currently everything is static within the `remote-components` directory. Eventually we'll push to npm and show things that way instead.