/*eslint no-console:0 */
'use strict'
require('core-js/fn/object/assign')
const express = require('express')
const cors = require('cors')
const fs = require('fs')
const app = express()
const router = express.Router()
const port = 5050

app.use(cors())
// app.use('/static', () => { })

// router.get('/', (req, res) => {
//   setTimeout(() => {
//     res.type('.js')
//     fs.createReadStream('remote-components/ReactRectanglePopupMenu.js').pipe(res)
//   }, 5000)
// })

// Some static stuff.
app.use('/ReactRectanglePopupMenu.js', router)

// Everything else?
app.use(express.static('remote-components'))

// Output to console.
app.listen(port, () => {
  console.clear('cleared')
  console.log(`Jukebox reloded: http://localhost:${port}!`)
})

/**
 * Flag indicating whether webpack compiled for the first time.
 * @type {boolean}
 */
let isInitialCompilation = true
