(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react')) :
  typeof define === 'function' && define.amd ? define(['exports', 'react'], factory) :
  (factory((global['@caasette'] = global['@caasette'] || {}, global['@caasette'].BlogPost = {}),global.React));
}(this, (function (exports,React) { 'use strict';

  React = React && React.hasOwnProperty('default') ? React['default'] : React;

  var buttonClick = function buttonClick() {
    return alert('Blog component!');
  };

  var index = (function () {
    return React.createElement("button", {
      onClick: function onClick() {
        return buttonClick();
      }
    }, "Button");
  });

  exports.default = index;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRXhhbXBsZUJsb2dQYWdlLmRldi5qcyIsInNvdXJjZXMiOlsiLi4vcGFja2FnZXMvYmxvZy1wYWdlL3NyYy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXG5cbmNvbnN0IGJ1dHRvbkNsaWNrID0gKCkgPT4gYWxlcnQoJ0Jsb2cgY29tcG9uZW50IScpXG5cbmV4cG9ydCBkZWZhdWx0ICgpID0+IChcbiAgPGJ1dHRvbiBvbkNsaWNrPXsoKSA9PiBidXR0b25DbGljaygpfT5cbiAgICBCdXR0b25cbiAgPC9idXR0b24+XG4pIl0sIm5hbWVzIjpbImJ1dHRvbkNsaWNrIiwiYWxlcnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0VBRUEsSUFBTUEsV0FBVyxHQUFHLFNBQWRBLFdBQWM7RUFBQSxTQUFNQyxLQUFLLENBQUMsaUJBQUQsQ0FBWDtFQUFBLENBQXBCOztBQUVBLGVBQWU7RUFBQSxTQUNiO0VBQVEsSUFBQSxPQUFPLEVBQUU7RUFBQSxhQUFNRCxXQUFXLEVBQWpCO0VBQUE7RUFBakIsY0FEYTtFQUFBLENBQWY7Ozs7Ozs7Ozs7OzsifQ==
