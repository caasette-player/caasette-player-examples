(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react')) :
  typeof define === 'function' && define.amd ? define(['exports', 'react'], factory) :
  (factory((global['@caasette'] = global['@caasette'] || {}, global['@caasette'].ReactRectanglePopupMenu = {}),global.React));
}(this, (function (exports,React) { 'use strict';

React = React && React.hasOwnProperty('default') ? React['default'] : React;

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css = ".PopupMenu_PopupMenu__8TfA4 {\n  position: relative; }\n\n.PopupMenu_button__20m_Y {\n  padding: 5px;\n  border-radius: 2px;\n  width: 30px;\n  height: 30px;\n  transition: background 0.25s ease-in-out; }\n  .PopupMenu_button__20m_Y:hover, .PopupMenu_button__20m_Y.active {\n    background: rgba(0, 0, 0, 0.1); }\n\n.PopupMenu_popover__3hd_Z {\n  background: white;\n  position: absolute;\n  width: calc(200px - 10px);\n  height: calc(200px - 10px);\n  left: calc(-100px + 50%);\n  top: 60px;\n  border-radius: 5px;\n  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n  transition: opacity 0.5s ease-in-out;\n  opacity: 0;\n  padding: 5px;\n  z-index: 1; }\n  .PopupMenu_popover__3hd_Z.active {\n    opacity: 1; }\n  .PopupMenu_popover__3hd_Z:hover + .PopupMenu_button__20m_Y {\n    background: rgba(0, 0, 0, 0.1); }\n  .PopupMenu_popover__3hd_Z:before {\n    content: \"\";\n    position: absolute;\n    display: block;\n    top: -20px;\n    left: calc(50% - 10px);\n    border: 10px solid white;\n    border-color: transparent transparent white transparent; }\n\n.PopupMenu_direction-top__1oqgB .PopupMenu_popover__3hd_Z {\n  top: auto;\n  bottom: 60px; }\n  .PopupMenu_direction-top__1oqgB .PopupMenu_popover__3hd_Z:before {\n    top: auto;\n    bottom: -20px;\n    border-color: white transparent transparent transparent; }\n\n.PopupMenu_direction-left__mSrJK .PopupMenu_popover__3hd_Z {\n  top: -100px;\n  left: auto !important;\n  right: 60px; }\n  .PopupMenu_direction-left__mSrJK .PopupMenu_popover__3hd_Z:before {\n    left: auto;\n    right: -20px;\n    top: calc(50% - 10px);\n    border-color: transparent transparent transparent white; }\n\n.PopupMenu_direction-right__yrXfz .PopupMenu_popover__3hd_Z {\n  top: -100px;\n  right: auto !important;\n  left: 60px !important; }\n  .PopupMenu_direction-right__yrXfz .PopupMenu_popover__3hd_Z:before {\n    right: auto;\n    left: -20px;\n    top: calc(50% - 10px);\n    border-color: transparent white transparent transparent; }\n";
var style = { "PopupMenu": "PopupMenu_PopupMenu__8TfA4", "button": "PopupMenu_button__20m_Y", "popover": "PopupMenu_popover__3hd_Z", "direction-top": "PopupMenu_direction-top__1oqgB", "direction-left": "PopupMenu_direction-left__mSrJK", "direction-right": "PopupMenu_direction-right__yrXfz" };
styleInject(css);

var Simple = function Simple(_ref) {
  var button = _ref.button,
      children = _ref.children;
  return React.createElement(
    'div',
    { className: style.PopupMenu },
    React.createElement(
      'h1',
      null,
      'THIS IS A simple web component.'
    )
  );
};

// export * from './PopupMenu';

exports.Simple = Simple;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ReactRectanglePopupMenu.js.map
