exports._test_remote_compnent_caasette = function (deps) {
  'use strict';

  // deps.link
  // deps.React
  // deps.Dom
  // deps._createClass
  // deps._classCallCheck
  // deps._possibleConstructorReturn
  // deps._inherits

  var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

  var Link = window.ReactRouterDOM.Link;

  var TestRemoteComponent = function (_React$Component) {
    _inherits(TestRemoteComponent, _React$Component);

    function TestRemoteComponent(props) {
      _classCallCheck(this, TestRemoteComponent);

      return _possibleConstructorReturn(this, (TestRemoteComponent.__proto__ || Object.getPrototypeOf(TestRemoteComponent)).call(this, props));
    }

    _createClass(TestRemoteComponent, [{
      key: 'render',
      value: function render() {
        return React.createElement(
          'div',
          null,
          'I am a remote component ',
          React.createElement(
            Link,
            { to: '/other' },
            'Go to other component'
          )
        );
      }
    }]);

    return TestRemoteComponent;
  }(React.Component);

  return {
    caasette: TestRemoteComponent,
    manifest: {
      cycle: true,
      name: 'some/name/here'
    }
  }
}
